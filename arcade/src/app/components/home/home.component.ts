import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent  {
  form: FormGroup = new FormGroup({});
  
  
  constructor(private fb: FormBuilder) { 
    this.form = fb.group({
      topic: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      phone: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      message: ['', [Validators.required]]
      
    });
  }
  
  ngOnInit(): void {
    
  }
  get f(){

    return this.form.controls;

  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.log(this.form.value);  }
}
